# Deploy Cluster K8S use container runtime CRIO

## Set up on all node 

1. Clone project to local machine
2. $ chmod +x install_crio_kubernetes.sh 
3. $ bash -x install_crio_kubernetes.sh  
## khoi tao cluster 

1. On Machine Master
```
$ sudo kubeadm init --apiserver-advertise-address=192.168.75.81 --pod-network-cidr=192.168.0.0/16
$ kubectl apply -f https://docs.projectcalico.org/v3.20/manifests/calico.yaml
$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
$ kubeadm token create --print-join-command

```

2. On Machine Worker 
```
$ sudo kubeadm join .....
```

3. Kiem tra cluster 
```
$ kubectl get node -o wide 
```